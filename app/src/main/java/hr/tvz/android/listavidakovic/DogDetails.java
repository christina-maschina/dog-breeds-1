package hr.tvz.android.listavidakovic;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import hr.tvz.android.listavidakovic.databinding.ActivityDogDetailsBinding;

public class DogDetails extends AppCompatActivity {

    private TextView titleView, descriptionView;
    private ImageView imageView;
    private String dataName, dataDescription;
    private int dataImage;
    private RatingBar ratingBar;
    private float userRating;
    private Menu menu;

    ActivityDogDetailsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDogDetailsBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        titleView = binding.title;
        descriptionView = binding.description;
        imageView = binding.img;
        ratingBar = binding.ratingBar;

        getData();
        setData();

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DogDetails.this, Image.class);
                intent.putExtra("image", dataImage);
                startActivity(intent);
            }
        });

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                userRating = rating;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.activity_menu, menu);
        this.menu = menu;
        for(int i = 0; i < menu.size(); i++){
            Drawable drawable = menu.getItem(i).getIcon();
            if(drawable != null) {
                drawable.mutate();
                drawable.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
            }
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        DialogFragment shareDialog = new ShareDialog(getApplicationContext(), userRating);
        shareDialog.show(getSupportFragmentManager(), "Share");
        return super.onOptionsItemSelected(item);
    }

    private void getData() {
        if (getIntent().hasExtra("dogObject")) {
            DogBreeds dogBreeds = getIntent().getParcelableExtra("dogObject");
            dataName = dogBreeds.getName();
            dataDescription = dogBreeds.getDescription();
            dataImage = dogBreeds.getImages();
        } else {
            Toast.makeText(this, "No data", Toast.LENGTH_SHORT).show();
        }
    }

    private void setData() {
        titleView.setText(dataName.toUpperCase());
        descriptionView.setText(dataDescription);
        imageView.setImageResource(dataImage);
    }

    public void openWebPage(View view) {
        String url = "https://www.countryliving.com/life/kids-pets/g3283/the-50-most-popular-dog-breeds/";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(DogDetails.this, "Can't display web page", Toast.LENGTH_SHORT).show();
        }

    }
}