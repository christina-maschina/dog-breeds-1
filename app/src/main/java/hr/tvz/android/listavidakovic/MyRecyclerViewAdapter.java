package hr.tvz.android.listavidakovic;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

    private List<DogBreeds> list;
    private String s2[];
    private int images[];
    private Context context;


    MyRecyclerViewAdapter(Context context, List<DogBreeds> list, String s2[], int images[]) {
        this.context = context;
        this.list = list;
        this.s2 = s2;
        this.images = images;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.activity_recyclerview_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        int color = setColor(holder, position);
        holder.dogNameText.setText(list.get(position).getName().toUpperCase());
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pressedColor=setColor(holder, position+1);
                holder.cardView.setCardBackgroundColor(pressedColor);

                Intent intent = new Intent(context, DogDetails.class);
                intent.putExtra("dogObject", list.get(position));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView dogNameText;
        ConstraintLayout mainLayout;
        CardView cardView;

        ViewHolder(View itemView) {
            super(itemView);
            dogNameText = itemView.findViewById(R.id.dog);
            cardView = itemView.findViewById(R.id.card);
            mainLayout = itemView.findViewById(R.id.mainLayout);
        }

    }

    private int setColor(ViewHolder holder, int position) {
        if (position % 2 == 0) {
            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.baby_blue));
            return ContextCompat.getColor(context, R.color.baby_blue);
        }
        holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.baby_green));
        return ContextCompat.getColor(context, R.color.baby_green);

    }

}
