package hr.tvz.android.listavidakovic;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class Image extends AppCompatActivity {
    private Animation animation;
    private ImageView imageView;
    private int dataImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        animation = AnimationUtils.loadAnimation(this, R.anim.animation);
        imageView=findViewById(R.id.img);
        dataImage=getIntent().getIntExtra("image", 1);
        imageView.setImageResource(dataImage);
        imageView.startAnimation(animation);
    }
}