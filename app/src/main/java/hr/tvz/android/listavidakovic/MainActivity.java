package hr.tvz.android.listavidakovic;


import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import hr.tvz.android.listavidakovic.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity{
    RecyclerView recyclerView;
    private ActivityMainBinding binding;

    String s1[], s2[];
    int images[]={R.drawable.labrador_retriever, R.drawable.german_shepherd, R.drawable.golden_retriever,
            R.drawable.bulldog, R.drawable.beagle, R.drawable.french_bulldog, R.drawable.yorkshire_terrier,
            R.drawable.poodle, R.drawable.rottweiler, R.drawable.boxer, R.drawable.german_shorthaired_pointer,
            R.drawable.siberian_husky, R.drawable.dachshund, R.drawable.doberman, R.drawable.great_dane,
            R.drawable.miniature_schnauzer, R.drawable.cavalier_king, R.drawable.shih_tzu, R.drawable.pemborke,
            R.drawable.pomeranian};
    private List<DogBreeds> list= new ArrayList<>();

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityMainBinding.inflate(getLayoutInflater());
        View view=binding.getRoot();
        setContentView(view);

        recyclerView=binding.recyclerView;

        s1=getResources().getStringArray(R.array.dogs);
        s2=getResources().getStringArray(R.array.description);

        createList(s1, s2, images);

        MyRecyclerViewAdapter adapter=new MyRecyclerViewAdapter(this,  list, s2, images);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        IntentFilter intentFilter=new IntentFilter();
        intentFilter.addAction(Intent.ACTION_HEADSET_PLUG);
        registerReceiver(new SystemReceiver(), intentFilter);
    }

    private void createList(String s1[], String s2[], int images[]) {
        int i=0;
        for (String s: s1) {
            list.add(new DogBreeds(s1[i], s2[i], images[i]));
            i++;
        }
    }

}