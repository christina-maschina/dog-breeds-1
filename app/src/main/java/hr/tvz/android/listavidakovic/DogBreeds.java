package hr.tvz.android.listavidakovic;

import android.os.Parcel;
import android.os.Parcelable;

public class DogBreeds implements Parcelable {
    private String name;
    private String description;
    private int images;

    public DogBreeds(String name, String description, int images) {
        this.name = name;
        this.description = description;
        this.images = images;
    }

    protected DogBreeds(Parcel in) {
        name = in.readString();
        description = in.readString();
        images = in.readInt();
    }

    public static final Creator<DogBreeds> CREATOR = new Creator<DogBreeds>() {
        @Override
        public DogBreeds createFromParcel(Parcel in) {
            return new DogBreeds(in);
        }

        @Override
        public DogBreeds[] newArray(int size) {
            return new DogBreeds[size];
        }
    };

    @Override
    public String toString() {
        return "DogBreeds{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", images=" + images +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeInt(images);
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getImages() {
        return images;
    }

    public static Creator<DogBreeds> getCREATOR() {
        return CREATOR;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setImages(int images) {
        this.images = images;
    }
}
